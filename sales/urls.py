from django.urls import path

from . import views

app_name = 'sales'
urlpatterns = [
        # Index
        path('', views.index, name='index'),
        # Sales
        path('sale/add/', views.SaleCreateView.as_view(), name='sale-create'),
        path('sale/<slug:slug>/view/', views.SaleDetailView.as_view(), name='sale-detail'),
        path('sale/<uuid:pk>/manage/', views.SaleManageView.as_view(), name='sale-manage'),
        path('sale/<uuid:pk>/update/', views.SaleUpdateView.as_view(), name='sale-update'),
        path('sale/<uuid:pk>/delete/', views.SaleDeleteView.as_view(), name='sale-delete'),
        # Items
        path('sale/<uuid:sale_id>/item/add/', views.ItemCreateView.as_view(), name='item-create'),
        path('item/<slug:slug>/view/', views.ItemDetailView.as_view(), name='item-detail'),
        path('item/<uuid:pk>/manage/', views.ItemManageView.as_view(), name='item-manage'),
        path('item/<uuid:pk>/update/', views.ItemUpdateView.as_view(), name='item-update'),
        path('item/<uuid:pk>/delete/', views.ItemDeleteView.as_view(), name='item-delete'),
        # Images
        path('item/<uuid:item_id>/image/add/', views.ItemImageCreateView.as_view(), name='image-create'),
        path('image/<uuid:pk>/update/', views.ItemImageUpdateView.as_view(), name='image-update'),
        path('image/<uuid:pk>/delete/', views.ItemImageDeleteView.as_view(), name='image-delete'),
]
