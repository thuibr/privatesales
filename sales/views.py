from django.forms import modelformset_factory
from django.shortcuts import get_object_or_404, render
from django.views.generic import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy

from .forms import ItemForm
from .models import Item, ItemImage, Sale


def index(request):
    return render(request, 'sales/index.html', {})


class SaleCreateView(CreateView):
    model = Sale
    fields = ['name']

class SaleManageView(DetailView):
    model = Sale
    template_name = 'sales/sale_manage.html'


class SaleUpdateView(UpdateView):
    model = Sale
    fields = ['name']


class SaleDeleteView(DeleteView):
    model = Sale
    success_url = reverse_lazy('sales:index')


class SaleDetailView(DetailView):
    model = Sale

class ItemManageView(DetailView):
    model = Item
    template_name = 'sales/item_manage.html'

class ItemSaleMixin:
    """Adds access to `Item` instance's `Sale` relation."""

    def get_success_url(self) -> str:
        """Redirect to related `Sale` instance's update page."""
        pk = self._get_sale_pk()
        return reverse_lazy('sales:sale-manage', kwargs={'pk': pk})

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        pk = self._get_sale_pk()
        context['sale_id'] = pk
        context['sale_name'] = Sale.objects.get(pk=pk)
        return context


class ItemCreateView(ItemSaleMixin, CreateView):
    model = Item
    fields = ['name', 'price', 'description']

    def dispatch(self, request, *args, **kwargs):
        """Overriden to make sure that `Sale` instance exists."""
        self.sale = get_object_or_404(Sale, pk=kwargs['sale_id'])
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        """Overriden to add the sale relation to the `Item` instance."""
        form.instance.sale = self.sale
        return super().form_valid(form)

    def _get_sale_pk(self):
        """Use saved `Sale` instance's `pk`."""
        return self.sale.pk


class ItemUpdateView(ItemSaleMixin, UpdateView):
    model = Item
    fields = ['name', 'price', 'description']

    def _get_sale_pk(self):
        """Use `Item` instance's related `Sale` instance for `pk`."""
        return self.get_object().sale.pk

    def get_success_url(self) -> str:
        """Redirect to related `Item` instance's update page."""
        pk = self.get_object().pk
        return reverse_lazy('sales:item-manage', kwargs={'pk': pk})


class ItemDeleteView(ItemSaleMixin, DeleteView):
    model = Item

    def _get_sale_pk(self):
        """Use `Item` instance's related `Sale` instance for `pk`."""
        return self.get_object().sale.pk


class ItemDetailView(DetailView):
    model = Item


class ItemMixin:
    """Adds access to `ItemImage` instance's `Item` relation."""

    def get_success_url(self) -> str:
        """Redirect to related `Item` instance's update page."""
        pk = self._get_item_pk()
        return reverse_lazy('sales:item-manage', kwargs={'pk': pk})

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['item_id'] = self._get_item_pk()
        return context


class ItemImageCreateView(ItemMixin, CreateView):
    model = ItemImage
    fields = ['image']

    def dispatch(self, request, *args, **kwargs):
        """Overriden to make sure that `Item` instance exists."""
        self.item = get_object_or_404(Item, pk=kwargs['item_id'])
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        """Overriden to add the item relation to the `ItemImage` instance."""
        form.instance.item = self.item
        return super().form_valid(form)

    def _get_item_pk(self):
        """Use saved `Item` instance's `pk`."""
        return self.item.pk


class ItemImageUpdateView(ItemMixin, UpdateView):
    model = ItemImage
    fields = ['image', 'primary']

    def _get_item_pk(self):
        """Use `ItemImage` instance's related `Image` instance for `pk`."""
        return self.get_object().item.pk


class ItemImageDeleteView(ItemMixin, DeleteView):
    model = ItemImage

    def _get_item_pk(self):
        """Use `ItemImage` instance's related `Item` instance for `pk`."""
        return self.get_object().item.pk