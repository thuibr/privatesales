from django.contrib import admin
from sorl.thumbnail.admin import AdminImageMixin

from . import models


class ItemInline(admin.TabularInline):
    model = models.Item
    exclude = ('slug',)

class SaleAdmin(admin.ModelAdmin):
    inlines = [
        ItemInline
    ]
    exclude = ('slug',)
    list_display = ('name', 'created', 'item_count')
    list_filter = ('created',)

class ImageInline(AdminImageMixin, admin.TabularInline):
    model = models.ItemImage

class ItemAdmin(admin.ModelAdmin):
    inlines = [
        ImageInline
    ]
    exclude = ('slug',)
    list_display = ('name', 'sale', 'image_count')

admin.site.register(models.Sale, SaleAdmin)
admin.site.register(models.Item, ItemAdmin)
