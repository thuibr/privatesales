import uuid

from django.core.validators import MinLengthValidator, MinValueValidator
from django.db import models
from django.db.models import CheckConstraint, Q
from django.db.models.functions import Length
from django.utils.text import slugify
from django.urls import reverse
from sorl.thumbnail import ImageField

class AbstractUUIDMixin(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    @property
    def pk_s(self):
        return str(self.pk)

    class Meta:
        abstract = True

models.CharField.register_lookup(Length)

class AbstractNamedUUIDMixin(AbstractUUIDMixin):
    slug = models.SlugField(unique=True, editable=False)
    name = models.CharField(max_length=50)

    class Meta:
        abstract = True

        constraints = [
            models.CheckConstraint(
                check=Q(name__length__gte=1),
                name="%(app_label)s_%(class)s_name_length",
            )
        ]

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        """Saves a unique slug for new objects."""
        if self.slug == '':
            # Get base slug name
            slug = slugify(self.name)

            # Update if already exists
            new_slug = slug
            num = 1
            while self.__class__.objects.filter(slug=new_slug).exists():
                new_slug = f"{slug}-{num}"
                num += 1

            # Finally, set it
            self.slug = new_slug

        super().save(*args, **kwargs)

class Sale(AbstractNamedUUIDMixin):
    created = models.DateTimeField(auto_now_add=True)


    def item_count(self):
        """Returns the number of items in the sale."""
        return self.items.count()

    def get_absolute_url(self):
        return reverse('sales:sale-manage', kwargs={'pk': self.pk})

class Item(AbstractNamedUUIDMixin):
    sale = models.ForeignKey('Sale', on_delete=models.CASCADE, related_name='items')
    price = models.DecimalField(max_digits=7, decimal_places=2, default=0.00, validators=[MinValueValidator(0.0, 'Price cannot be less than zero.')])
    description = models.TextField(blank=True)

    class Meta:
        constraints = AbstractNamedUUIDMixin.Meta.constraints + [CheckConstraint(check=Q(price__gte=0.00), name="item_price_minimum")]

    def image_count(self):
        """Returns the number of images in the sale."""
        return self.images.count()

    def get_absolute_url(self):
        return reverse('sales:item-manage', kwargs={'pk': self.pk})


class ItemImage(AbstractUUIDMixin):
    item = models.ForeignKey('Item', on_delete=models.CASCADE, related_name='images')
    image = ImageField(upload_to="images/")
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'item="{self.item}" path="{self.image.name}"'
