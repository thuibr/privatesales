
from django.db.utils import IntegrityError
from django.test import TestCase

from .utils import create_item, create_sale


class TestSale(TestCase):
    def test_unique_slugs(self):
        "A unique slug gets created upon save."
        name = "My Sale"
        for i in range(1, 4):
            expected_slug = "my-sale"
            if i > 1:
                expected_slug += f"-{i-1}"
            sale = create_sale(name)
            self.assertEqual(sale.slug, expected_slug)

    def test_slug_does_not_change(self):
        """Slug should not change after name change."""
        sale = create_sale("My Sale")
        slug = sale.slug
        sale.name = 'My New Sale Name'
        sale.save()
        self.assertEqual(slug, sale.slug)

    def test_blank_name_invalid(self):
        """A blank name is invalid."""
        with self.assertRaises(IntegrityError):
            s = create_sale(name='')


class TestItem(TestCase):
    def test_unique_slugs(self):
        "A unique slug gets created upon save."
        sale = create_sale("My Sale")
        name = "My Item"
        for i in range(1, 4):
            expected_slug = "my-item"
            if i > 1:
                expected_slug += f"-{i-1}"
            item = create_item(name, sale)
            self.assertEqual(item.slug, expected_slug)

    def test_slug_does_not_change(self):
        """Slug should not change after name change."""
        sale = create_sale("My Sale")
        item = create_item("My Item", sale)
        slug = item.slug
        item.name = 'My New Item Name'
        item.save()
        self.assertEqual(slug, item.slug)
    
    def test_non_negative_prices(self):
        "Prices cannot be less than 0."
        sale = create_sale("My Sale")
        item = create_item("My Item", sale)
        with self.assertRaises(IntegrityError):
            item.price = -1.0
            item.save()

    def test_blank_name_invalid(self):
        """A blank name is invalid."""
        sale = create_sale("My Sale")
        with self.assertRaises(IntegrityError):
            item = create_item("", sale)
