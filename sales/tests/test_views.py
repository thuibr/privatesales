import os

from django.test import TestCase
from django.urls import reverse

from sales.models import Sale
from sales.tests.utils import create_itemimage, create_item, create_sale


class TestPrivateIdsNotExposed(TestCase):
    "Private IDs of records should not be exposed in public views."

    def setUp(self):
        "Setup some objects for testing."
        if Sale.objects.count() > 0:
            import pdb; pdb.set_trace()
        self.sale = create_sale('My Sale')
        self.item = create_item('My Item', self.sale)
        img_path = os.path.join(os.path.dirname(__file__), 'files', 'table.jpeg')
        self.image = create_itemimage(img_path, self.item)
    
    def tearDown(self):
        "Delete image after running test."
        self.image.image.delete()
    
    def assert_not_contains_private_ids(self, url):
        "Calls `url` and checks that both `url` and response don't contain ids."
        response = self.client.get(url)

        for pk in [self.sale.pk_s, self.item.pk_s, self.image.pk_s]:
            self.assertNotContains(response, pk)
            self.assertNotIn(pk, url)
    
    def test_sale_detail(self):
        "No private ids should be in the sale detail view."
        url = reverse('sales:sale-detail', kwargs={'slug': self.sale.slug})
        self.assert_not_contains_private_ids(url)

    def test_item_detail(self):
        "No private ids should be in the item detail view."
        url = reverse('sales:item-detail', kwargs={'slug': self.item.slug})
        self.assert_not_contains_private_ids(url)
