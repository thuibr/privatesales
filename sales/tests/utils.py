from tempfile import TemporaryFile

from django.core.files import File

from ..models import Item, ItemImage, Sale


def create_sale(name):
    "Returns a `Sale` instance."
    sale = Sale(name=name)
    sale.save()
    return sale

def create_item(name, sale, price=0):
    "Returns an `Item` instance."
    item = Item(name=name, sale=sale, price=price)
    item.save()
    return item

def create_itemimage(img_path, item):
    "Returns an `ItemImage` instance."
    with open(img_path, 'rb') as input_file:
        temp_file = TemporaryFile()
        # with open(temp_file, 'w'):
        temp_file.write(input_file.read())
        img_file = File(temp_file, 'test_file.jpeg')

        item_image = ItemImage(item=item, image=img_file)
        item_image.save()

        return item_image