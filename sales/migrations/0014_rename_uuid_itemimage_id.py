# Generated by Django 4.0.4 on 2022-05-17 23:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0013_remove_itemimage_id_alter_itemimage_uuid'),
    ]

    operations = [
        migrations.RenameField(
            model_name='itemimage',
            old_name='uuid',
            new_name='id',
        ),
    ]
